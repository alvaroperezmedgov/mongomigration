const mongoose = require('mongoose')

const pymsdb = mongoose.connection.useDb('pyms');
pymsdb.config.useCreateIndex = true;

const newpymsdb = mongoose.connection.useDb('newPyms');
newpymsdb.config.useCreateIndex = true;

module.exports.pymsdb = pymsdb;
module.exports.newpymsdb = newpymsdb;
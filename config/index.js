const config = {
  url: '10.1.222.200',
  port: process.env.MONGODB_PORT || 27017,
  database: process.env.MONGODB_DB || 'pyms',
  username: process.env.MONGODB_USER || 'admin',
  password: process.env.MONGODB_PASSWORD || 'admin',

  secretKey: process.env.NXAUTH_SECRET_KEY || 'llave_secreta',
  projectId: process.env.NXAUTH_PROJECT_ID || '',
  issuer: '', // domain who generate the JWT
  audience: '', // domains that can generate JWT
  expiresIn: '7d' // 7 days
}

module.exports = config

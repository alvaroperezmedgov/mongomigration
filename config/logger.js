const { createLogger, format, transports } = require('winston');
const { combine, timestamp, prettyPrint } = format;

const now = new Date();
const dateNow = `${now.getFullYear()}-${now.getMonth() + 1}-${now.getDate()} `

exports.loggerEntities = createLogger({
  level: 'info',
  format: combine(timestamp(), prettyPrint()),
  transports: [
    new transports.File({ filename: `logs/entities-${dateNow}.log` }),
  ],
});

exports.loggerPersons = createLogger({
  level: 'info',
  format: combine(timestamp(), prettyPrint()),
  transports: [
    new transports.File({ filename: `logs/persons-${dateNow}.log` }),
  ],
});

exports.loggerBillings = createLogger({
  level: 'info',
  format: combine(timestamp(), prettyPrint()),
  transports: [
    new transports.File({ filename: `logs/billings-${dateNow}.log` }),
  ],
});

exports.loggerNutritions = createLogger({
  level: 'info',
  format: combine(timestamp(), prettyPrint()),
  transports: [
    new transports.File({ filename: `logs/nutritions-${dateNow}.log` }),
  ],
});

exports.loggerSurveys = createLogger({
  level: 'info',
  format: combine(timestamp(), prettyPrint()),
  transports: [
    new transports.File({ filename: `logs/surveys-${dateNow}.log` }),
  ],
});

exports.loggerCanalizations = createLogger({
  level: 'info',
  format: combine(timestamp(), prettyPrint()),
  transports: [
    new transports.File({ filename: `logs/canalizations-${dateNow}.log` }),
  ],
});

exports.loggerContracts = createLogger({
  level: 'info',
  format: combine(timestamp(), prettyPrint()),
  transports: [
    new transports.File({ filename: `logs/contracts-${dateNow}.log` }),
  ],
});

exports.loggerHealthRecords = createLogger({
  level: 'info',
  format: combine(timestamp(), prettyPrint()),
  transports: [
    new transports.File({ filename: `logs/healthRecords-${dateNow}.log` }),
  ],
});
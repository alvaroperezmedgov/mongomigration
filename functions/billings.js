const { pymsdb } = require("../config/connections");
const mongoose = require('mongoose')
const { loggerBillings } = require("../config/logger");
const Persons = require('../models/persons');
const NewBillings = require('../models/newBillings');

const billingsSchema = new mongoose.Schema({}, { strict: false });
const Billings = pymsdb.model('billings', billingsSchema);

const delArr = ["_id", "createdAt", "updatedAt", "__v"];

const billingsMigration = async () => {
  console.log('billings');
  let billings = await Billings.find().limit(10);

  for (let index = 0; index < billings.length; index++) {
    const element = billings[index];
    let obj = JSON.parse(JSON.stringify(element));
    let person = await Persons.findById(obj.person._id);
    let idPerson;

    if (person) {
      //console.log(person);
      idPerson = obj.person._id;
    } else {
      var newPerson = new Persons(obj.person);
      newPerson = await newPerson.save();
      idPerson = newPerson._id;
    }

    obj.person = idPerson;

    let billingId = obj._id;

    delArr.forEach(delElement => {
      delete obj[delElement];
    });

    var newBilling = new NewBillings(obj);
    newBilling = await newBilling.save();
    loggerBillings.info({
      billingId: billingId,
      newBillingId: newBilling._id,
      message: 'Creado'
    });
    console.log(`Migrate id ${billingId} to ${newBilling._id}`);
  }
}

exports.billingsMigration = billingsMigration;
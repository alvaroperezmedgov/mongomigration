const mongoose = require('mongoose');
const { pymsdb, newpymsdb } = require("../config/connections");
const { loggerCanalizations } = require("../config/logger");
const Schema = mongoose.Schema;
const NewCanalizations = require('../models/newCanalizations');

const canalizationsSchema = new mongoose.Schema({}, { strict: false });
const canalizationsFollowups = new Schema({
  canalizationId: { type: Schema.Types.ObjectId, ref: 'canalizations', index: true },
}, { strict: false });

const Canalizations = pymsdb.model('canalizations', canalizationsSchema);
const CanalizationsFollowups = newpymsdb.model('canalizationsFollowups', canalizationsFollowups);

const delArr = ["_id", "createdAt", "updatedAt", "__v"];

const canalizationsMigration = async () => {
  console.log('canalizations');
  let canalizations = await Canalizations.find(/* { followup: { $exists: true } } */).limit(100);

  for (let index = 0; index < canalizations.length; index++) {

    const element = canalizations[index];

    let obj = JSON.parse(JSON.stringify(element));

    let newData = {}
    let newFollowup = {}
    for (const property in obj) {
      let tipo = typeof obj[property];
      if (tipo !== 'object') {
        newData[property] = obj[property];
      } else {
        if (property == "followup") {
          newFollowup = obj[property];
        } else {
          let propObj = JSON.parse(JSON.stringify(obj[property]));
          for (const objProperty in propObj) {
            if (objProperty != "appointmentDate") {
              let newPropertyName = property + objProperty.replace(/^\w/, (c) => c.toUpperCase());
              newData[newPropertyName] = propObj[objProperty];
            } else {
              newData[objProperty] = propObj[objProperty];
            }
          }
        }
      }
    }


    delArr.forEach(delElement => {
      delete newData[delElement];
    });

    var newCanalization = new NewCanalizations(newData);
    newCanalization = await newCanalization.save();

    if (Object.keys(newFollowup).length) {
      newFollowup.canalizationId = newCanalization._id
      const newCanalizationFollowup = new CanalizationsFollowups(newFollowup);
      newCanalizationFollowup.save();
    }

    loggerCanalizations.info({
      canalizationId: obj._id,
      newCanalizationId: newCanalization._id,
      message: 'Creado'
    });
    console.log(`Migrate id ${obj._id} to ${newCanalization._id}`);
  }
}

exports.canalizationsMigration = canalizationsMigration;
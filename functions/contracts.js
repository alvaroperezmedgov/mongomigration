const mongoose = require('mongoose');
const { pymsdb, newpymsdb } = require("../config/connections");
const { loggerContracts } = require("../config/logger");
const Schema = mongoose.Schema;

const contractsSchema = new mongoose.Schema({}, { strict: false });
const newContractsSchema = new mongoose.Schema({
  HCType: { type: String, required: true },
  epsCode: { type: String },
}, { strict: false, timestamps: true });
const contractsActivitiesSchema = new Schema({
  contractId: { type: Schema.Types.ObjectId, ref: 'contracts', index: true },
}, { strict: false });
const contractsBillingItemsSchema = new Schema({
  contractActivityId: { type: Schema.Types.ObjectId, ref: 'contractsActivities', index: true },
}, { strict: false });

const Contracts = pymsdb.model('contracts', contractsSchema);
const NewContracts = newpymsdb.model('contracts', newContractsSchema);
const ContractsActivities = newpymsdb.model('contractsActivities', contractsActivitiesSchema);
const ContractsBillingItems = newpymsdb.model('contractsBillingItems', contractsBillingItemsSchema);

const delArr = ["_id", "createdAt", "updatedAt", "__v"];

const contractsMigration = async () => {
  console.log('contracts');
  let contracts = await Contracts.find().limit(100);

  for (let index = 0; index < contracts.length; index++) {

    const element = contracts[index];

    let obj = JSON.parse(JSON.stringify(element));

    let newData = {}
    let newActivities = {}

    for (const property in obj) {
      let tipo = typeof obj[property];
      if (tipo !== 'object') {
        newData[property] = obj[property];
      } else {
        if (property == "cityActivities") {
          newActivities = obj.cityActivities.default.activities;
        }
      }
    }

    delArr.forEach(delElement => {
      delete newData[delElement];
    });

    var newContracts = new NewContracts(newData);
    newContracts = await newContracts.save();

    if (Object.keys(newActivities).length) {
      for (const key in newActivities) {
        if (newActivities.hasOwnProperty(key)) {
          const activityElement = newActivities[key];
          let activityObj = {
            type: key,
            name: activityElement.name,
            contractId: newContracts._id
          }
          var newActivity = new ContractsActivities(activityObj);
          newActivity = await newActivity.save();
          let itemsToSave = activityElement.billingItems;
          if (itemsToSave.length) {
            itemsToSave.forEach(elemSave => {
              elemSave.contractActivityId = newActivity._id;
              var newBillingItem = new ContractsBillingItems(elemSave);
              newBillingItem.save();
            });
          }
        }
      }
    }

    loggerContracts.info({
      contractId: obj._id,
      newContractId: newContracts._id,
      message: 'Creado'
    });
    console.log(`Migrate id ${obj._id} to ${newContracts._id}`);
  }
}

exports.contractsMigration = contractsMigration;
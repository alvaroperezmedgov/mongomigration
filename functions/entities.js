const mongoose = require('mongoose');
const { loggerEntities } = require("../config/logger");
const { newpymsdb } = require("../config/connections");
const Entitiesf = require('../models/entities');
const EntitiesEquivalence = require('../models/entitiesEquivalence');

const newEntitieSchema = new mongoose.Schema({}, { strict: false });
const entitiesInformationSchema = new mongoose.Schema({}, { strict: false });
const entityGroupsSchema = new mongoose.Schema({}, { strict: false });
const entityEducationalModalitiesSchema = new mongoose.Schema({}, { strict: false });
const entitySchedulesSchema = new mongoose.Schema({}, { strict: false });
const visitsSchema = new mongoose.Schema({}, { strict: false });
const phonesSchema = new mongoose.Schema({}, { strict: false });
const characterizationSchema = new mongoose.Schema({}, { strict: false });
const familyMembersSchema = new mongoose.Schema({}, { strict: false });
const entitySurveysSchema = new mongoose.Schema({}, { strict: false });

const NewEntities = newpymsdb.model('entities', newEntitieSchema);
const EntitiesInformation = newpymsdb.model('entitiesInformation', entitiesInformationSchema);
const EntityGroups = newpymsdb.model('entitiesGroups', entityGroupsSchema);
const EntityEducationalModalities = newpymsdb.model('entitiesEducationalModalities', entityEducationalModalitiesSchema);
const EntitySchedules = newpymsdb.model('entitiesSchedules', entitySchedulesSchema);
const EntityVisits = newpymsdb.model('entitiesVisits', visitsSchema);
const EntityPhones = newpymsdb.model('entitiesPhones', phonesSchema);
const EntityCharacterizations = newpymsdb.model('entitiesCharacterizations', characterizationSchema);
const EntityFamilyCharacterizations = newpymsdb.model('entitiesFamilyCharacterizations', familyMembersSchema);
const EntitySurveys = newpymsdb.model('entitiesSurveysCharacterizations', entitySurveysSchema);

const delArr = ["_id", "createdAt", "updatedAt", "__v"];

const entitiesMigration = async () => {
  console.log('entidades');
  let entities = await Entitiesf.find().limit(100);

  for (let index = 0; index < entities.length; index++) {
    const element = entities[index];

    let obj = JSON.parse(JSON.stringify(element));
    const newData = {}
    var newInformation = {};

    var groupsArr = [];
    var educationalModalitiesArr = [];
    var schedulesArr = [];

    var visits = [];
    var characterizations = [];
    var phonesArr = [];

    var familyMembersArr = [];
    var surveysObj = {};

    for (const property in obj) {
      if (!Array.isArray(obj[property])) {
        let tipo = typeof obj[property];
        if (tipo !== 'object') {
          //console.log(`${property}`);
          newData[property] = obj[property];
        } else {
          if (property == 'information') {
            //recorrer information
            let informationObj = JSON.parse(JSON.stringify(obj[property]));
            for (const infoProperty in informationObj) {
              if (!Array.isArray(informationObj[infoProperty])) {
                newInformation[infoProperty] = informationObj[infoProperty];
              } else {
                switch (infoProperty) {
                  case 'groups':
                    groupsArr = informationObj[infoProperty];
                    break;
                  case 'educationalModalities':
                    educationalModalitiesArr = informationObj[infoProperty];
                    break;
                  case 'schedules':
                    schedulesArr = informationObj[infoProperty];
                    break;
                  default:
                    break;
                }
              }
            }
          }
        }
      } else {
        switch (property) {
          case 'visits':
            visits = obj[property];
            break;
          case 'characterizations':
            characterizations = obj[property];
            break;
          case 'phoneNumbers':
            phonesArr = obj[property];
            break;
          default:
            break;
        }
      }
    }

    delArr.forEach(delElement => {
      delete newData[delElement];
    });

    //guardar primer nivel de la entidad
    var newEntities = new NewEntities(newData);
    newEntities = await newEntities.save();
    loggerEntities.info({
      entityId: obj._id,
      newEntityId: newEntities._id,
      message: 'Creado'
    });
    console.log(`Migrate id ${obj._id} to ${newEntities._id}`);
    var newEntityEquivalence = new EntitiesEquivalence({
      oldEntityId: obj._id,
      newEntityId: newEntities._id
    });
    await newEntityEquivalence.save();

    //console.log(newEntities._id);
    //agregar id de nueva entidad a nueva informacion
    newInformation.idEntity = newEntities._id;
    //guardar informacion
    const entityInformation = new EntitiesInformation(newInformation);
    await entityInformation.save();
    //console.log(entityInformation);
    if (visits.length) {
      visits.forEach(async visit => {
        visit.idEntity = newEntities._id;
        const newVisit = new EntityVisits(visit);
        await newVisit.save();
      });
    }
    if (phonesArr.length) {
      for (const property in phonesArr) {
        let mObj = {
          typePhone: property,
          phone: phonesArr[property],
          idEntity: newEntities._id
        }
        const newPhone = new EntityPhones(mObj);
        await newPhone.save();
      }
    }
    if (groupsArr.length) {
      groupsArr.forEach(async group => {
        let groupObj = {
          group: group,
          idEntityInformation: entityInformation._id
        }
        const newGroup = new EntityGroups(groupObj);
        await newGroup.save();
      });
    }
    if (educationalModalitiesArr.length) {
      educationalModalitiesArr.forEach(async modality => {
        let mObj = {
          modality: modality,
          idEntityInformation: entityInformation._id
        }
        const newEducationalModalities = new EntityEducationalModalities(mObj);
        await newEducationalModalities.save();
      });
    }
    if (schedulesArr.length) {
      schedulesArr.forEach(async schedule => {
        let mObj = {
          schedule: schedule,
          idEntityInformation: entityInformation._id
        }
        const newSchedules = new EntitySchedules(mObj);
        await newSchedules.save();
      });
    }
  }
  const newCharData = {}
  if (characterizations) {
    if (characterizations.length) {
      characterizations.forEach(async characterization => {
        let characterizationObj = JSON.parse(JSON.stringify(characterization));
        for (const charProperty in characterizationObj) {
          let tipo = typeof characterizationObj[charProperty];
          if (tipo !== 'object') {
            newCharData[charProperty] = characterizationObj[charProperty];
          } else {
            //if (charProperty == "createdBy") {
            let obj = characterizationObj[charProperty];
            for (const key in obj) {
              if (obj.hasOwnProperty(key)) {
                let ntipo = typeof obj[key];
                if (!Array.isArray(obj[key]) && ntipo !== 'object') {
                  newCharData[key] = obj[key];
                } else {
                  switch (key) {
                    case "familyMembers":
                      familyMembersArr = obj[key];
                      break;
                    case "survey":
                      surveysObj = obj[key];
                      break;
                    default:
                      break;
                  }
                }
              }
            }
            //}
          }
        }

        newCharData.idEntity = newEntities._id

        const newCharacterization = new EntityCharacterizations(newCharData);
        await newCharacterization.save();

        if (surveysObj.length) {
          for (const property in surveysObj) {
            let mObj = {
              typeSurvey: property,
              data: surveysObj[property],
              idCharacterization: newCharacterization._id
            }
            const newSurvey = new EntitySurveys(mObj);
            newSurvey.save();
          }
        }

        if (familyMembersArr.length) {
          familyMembersArr.forEach(async family => {
            family.idCharacterization = newCharacterization._id;
            const newFamilyMember = new EntityFamilyCharacterizations(family);
            await newFamilyMember.save();
          });
        }
      });
    }
  }


}

exports.entitiesMigration = entitiesMigration;
const { pymsdb, newpymsdb } = require("../config/connections");
const mongoose = require('mongoose')

const generalSchema = new mongoose.Schema({}, { strict: false });
const Countries = pymsdb.model('countries', generalSchema);
const NewCountries = newpymsdb.model('countries', generalSchema);
const Projects = pymsdb.model('projects', generalSchema);
const NewProjects = newpymsdb.model('projects', generalSchema);
const Signatures = pymsdb.model('signatures', generalSchema);
const NewSignatures = newpymsdb.model('signatures', generalSchema);
const StudentsGroups = pymsdb.model('studentsGroups', generalSchema);
const NewStudentsGroups = newpymsdb.model('studentsGroups', generalSchema);
const Users = pymsdb.model('users', generalSchema);
const NewUsers = newpymsdb.model('users', generalSchema);

const equalCollections = async () => {
  console.log('countries');
  let countries = await Countries.find().limit(10);
  for (let index = 0; index < countries.length; index++) {
    const element = countries[index];
    let obj = JSON.parse(JSON.stringify(element));
    var country = new NewCountries(obj);
    country = await country.save();
    console.log(`Migrate id ${obj._id} to ${country._id}`);
  }
  console.log('projects');
  let projects = await Projects.find().limit(10);
  for (let index = 0; index < projects.length; index++) {
    const element = projects[index];
    let obj = JSON.parse(JSON.stringify(element));
    var project = new NewProjects(obj);
    project = await project.save();
    console.log(`Migrate id ${obj._id} to ${project._id}`);
  }
  console.log('signatures');
  let signatures = await Signatures.find().limit(10);
  for (let index = 0; index < signatures.length; index++) {
    const element = signatures[index];
    let obj = JSON.parse(JSON.stringify(element));
    var sign = new NewSignatures(obj);
    sign = await sign.save();
    console.log(`Migrate id ${obj._id} to ${sign._id}`);
  }
  console.log('studentsGroups');
  let studentsGroups = await StudentsGroups.find().limit(10);
  for (let index = 0; index < studentsGroups.length; index++) {
    const element = studentsGroups[index];
    let obj = JSON.parse(JSON.stringify(element));
    var studentsGroup = new NewStudentsGroups(obj);
    studentsGroup = await studentsGroup.save();
    console.log(`Migrate id ${obj._id} to ${studentsGroup._id}`);
  }
  console.log('users');
  let users = await Users.find().limit(10);
  for (let index = 0; index < users.length; index++) {
    const element = users[index];
    let obj = JSON.parse(JSON.stringify(element));
    var user = new NewUsers(obj);
    user = await user.save();
    console.log(`Migrate id ${obj._id} to ${user._id}`);
  }

}

exports.equalCollections = equalCollections;
const mongoose = require('mongoose');
const { pymsdb, newpymsdb } = require("../config/connections");
const { loggerHealthRecords } = require("../config/logger");
const Schema = mongoose.Schema;
const Persons = require('../models/persons');
const NewHealthRecords = require('../models/newHealthRecords');
const HealthRecordsEquivalence = require('../models/healthRecordsEquivalence');

const healthRecordsSchema = new Schema({}, { strict: false });
const healthRecordsSupportsSchema = new Schema({
  healthRecordsId: { type: Schema.Types.ObjectId, ref: 'newHealthRecords' },
  type: { type: String },
  file: { type: String },
}, { strict: false });

const healthRecordsPersonalHistorySchema = new Schema({
  healthRecordsId: { type: Schema.Types.ObjectId, ref: 'newHealthRecords' },
}, { strict: false });
const healthRecordsFamilyHistorySchema = new Schema({
  healthRecordsId: { type: Schema.Types.ObjectId, ref: 'newHealthRecords' },
}, { strict: false });
const healthRecordsSexualitySchema = new Schema({
  healthRecordsId: { type: Schema.Types.ObjectId, ref: 'newHealthRecords' },
}, { strict: false });
const healthRecordsGynecoObstetricHistorySchema = new Schema({
  healthRecordsId: { type: Schema.Types.ObjectId, ref: 'newHealthRecords' },
}, { strict: false });
const healthRecordscontraceptiveMethodsSchema = new Schema({
  healthRecordsId: { type: Schema.Types.ObjectId, ref: 'newHealthRecords' },
}, { strict: false });
const healthRecordsPhysicalExamSchema = new Schema({
  healthRecordsId: { type: Schema.Types.ObjectId, ref: 'newHealthRecords' },
}, { strict: false });
const healthRecordsIndividualEducationSchema = new Schema({
  healthRecordsId: { type: Schema.Types.ObjectId, ref: 'newHealthRecords' },
}, { strict: false });
const HealthRecordsReviewSchema = new Schema({
  healthRecordsId: { type: Schema.Types.ObjectId, ref: 'newHealthRecords' },
}, { strict: false });
const HealthRecordsInterventionsSchema = new Schema({
  healthRecordsId: { type: Schema.Types.ObjectId, ref: 'newHealthRecords' },
}, { strict: false });
const HealthRecordsSampleSchema = new Schema({
  healthRecordsId: { type: Schema.Types.ObjectId, ref: 'newHealthRecords' },
}, { strict: false });

const HealthRecords = pymsdb.model('healthRecords', healthRecordsSchema);
const HealthRecordsSupports = newpymsdb.model('healthRecordsSupports', healthRecordsSupportsSchema);
const HealthRecordsPersonalHistory = newpymsdb.model('healthRecordsPersonalHistory', healthRecordsPersonalHistorySchema);
const HealthRecordsFamilyHistory = newpymsdb.model('healthRecordsFamilyHistories', healthRecordsFamilyHistorySchema);
const HealthRecordsSexuality = newpymsdb.model('healthRecordsSexualities', healthRecordsSexualitySchema);
const HealthRecordsGynecoObstetricHistory = newpymsdb.model('healthRecordsGynecoObstetricHistories', healthRecordsGynecoObstetricHistorySchema);
const HealthRecordscontraceptiveMethods = newpymsdb.model('healthRecordscontraceptiveMethods', healthRecordscontraceptiveMethodsSchema);
const HealthRecordsPhysicalExam = newpymsdb.model('healthRecordsPhysicalExams', healthRecordsPhysicalExamSchema);
const HealthRecordsIndividualEducation = newpymsdb.model('healthRecordsIndividualEducations', healthRecordsIndividualEducationSchema);
const HealthRecordsReview = newpymsdb.model('healthRecordsReviews', HealthRecordsReviewSchema);
const HealthRecordsInterventions = newpymsdb.model('healthRecordsInterventions', HealthRecordsInterventionsSchema);
const HealthRecordsSamples = newpymsdb.model('healthRecordsSamples', HealthRecordsSampleSchema);

const delArr = ["_id", "createdAt", "updatedAt", "__v"];

const healthRecordsMigration = async () => {
  console.log('healthRecords');
  let healthRecords = await HealthRecords.find().limit(100);

  for (let index = 0; index < healthRecords.length; index++) {
    const element = healthRecords[index];
    let obj = JSON.parse(JSON.stringify(element));
    let newData = {}
    let supports = []
    let objectsToMigrate = {}

    for (const property in obj) {
      let tipo = typeof obj[property];
      if (tipo !== 'object') {
        newData[property] = obj[property];
      } else {
        if (property == "data") {
          for (const key in obj.data) {
            if (obj.data.hasOwnProperty(key)) {
              const objElement = obj.data[key];
              switch (key) {
                case "reasonConsultation":
                case "process":
                case "resultTest":
                  let tipo2 = typeof obj.data[key];
                  if (tipo2 !== 'object') {
                    newData[key] = objElement;
                  } else {
                    for (const keyelement in objElement) {
                      if (objElement.hasOwnProperty(keyelement)) {
                        const newElement = objElement[keyelement];
                        newData[keyelement] = newElement;
                      }
                    }
                  }
                  break;
                case "personalHistory":
                case "familyHistory":
                case "sexuality":
                case "gynecoObstetricHistory":
                case "contraceptiveMethods":
                case "physicalExam":
                case "IndividualEducation":
                case "review":
                case "interventions":
                case "sample":
                  let tipo3 = typeof obj.data[key];
                  if (tipo3 == 'object') {
                    objectsToMigrate[key] = objElement;
                  }
                  break;
                default:
                  loggerHealthRecords.error({
                    id: obj._id,
                    key: key,
                    message: 'Valor no organizado'
                  });
                  break;
              }
            }
          }
        } else if (property == "supports") {
          var supportsObj = obj[property];
          for (const key in supportsObj) {
            if (supportsObj.hasOwnProperty(key)) {
              const elementArr = supportsObj[key];
              elementArr.forEach(supportsElement => {
                supports.push({
                  type: key,
                  file: supportsElement
                })
              });
            }
          }
        }
      }
    }

    let person = await Persons.findById(obj.person._id);
    let idPerson;
    if (person) {
      idPerson = obj.person._id;
    } else {
      var newPerson = new Persons(obj.person);
      newPerson = await newPerson.save();
      idPerson = newPerson._id;
    }

    newData.person = idPerson;
    let healthRecordsId = obj._id;

    delArr.forEach(delElement => {
      delete newData[delElement];
    });

    var newHealthRecord = new NewHealthRecords(newData);
    newHealthRecord = await newHealthRecord.save();

    if (supports.length) {
      supports.forEach(async (supportsElement) => {
        supportsElement.healthRecordsId = newHealthRecord._id
        var newSupportsHealthRecord = new HealthRecordsSupports(supportsElement);
        await newSupportsHealthRecord.save();
      });
    }

    if (Object.keys(objectsToMigrate).length) {
      for (const key in objectsToMigrate) {
        if (objectsToMigrate.hasOwnProperty(key)) {
          const objectsToMigrateElement = objectsToMigrate[key];
          objectsToMigrateElement.healthRecordsId = newHealthRecord._id;
          switch (key) {
            case "personalHistory":
              var newPersonalHistory = new HealthRecordsPersonalHistory(objectsToMigrateElement);
              newPersonalHistory = await newPersonalHistory.save();
              break;
            case "familyHistory":
              var newFamilyHistory = new HealthRecordsFamilyHistory(objectsToMigrateElement);
              newFamilyHistory = await newFamilyHistory.save();
              break;
            case "sexuality":
              var newsexuality = new HealthRecordsSexuality(objectsToMigrateElement);
              newsexuality = await newsexuality.save();
              break;
            case "gynecoObstetricHistory":
              var newgynecoObstetricHistory = new HealthRecordsGynecoObstetricHistory(objectsToMigrateElement);
              newgynecoObstetricHistory = await newgynecoObstetricHistory.save();
              break;
            case "contraceptiveMethods":
              var newcontraceptiveMethods = new HealthRecordscontraceptiveMethods(objectsToMigrateElement);
              newcontraceptiveMethods = await newcontraceptiveMethods.save();
              break;
            case "physicalExam":
              var newphysicalExam = new HealthRecordsPhysicalExam(objectsToMigrateElement);
              newphysicalExam = await newphysicalExam.save();
              break;
            case "IndividualEducation":
              var newIndividualEducation = new HealthRecordsIndividualEducation(objectsToMigrateElement);
              newIndividualEducation = await newIndividualEducation.save();
              break;
            case "review":
              var newreview = new HealthRecordsReview(objectsToMigrateElement);
              newreview = await newreview.save();
              break;
            case "interventions":
              var newinterventions = new HealthRecordsInterventions(objectsToMigrateElement);
              newinterventions = await newinterventions.save();
              break;
            case "sample":
              var newsamples = new HealthRecordsSamples(objectsToMigrateElement);
              newsamples = await newsamples.save();
              break;
            default:
              break;
          }
        }
      }
    }

    loggerHealthRecords.info({
      healthRecordsId: healthRecordsId,
      newHealthRecordsId: newHealthRecord._id,
      message: 'Creado'
    });
    console.log(`Migrate id ${healthRecordsId} to ${newHealthRecord._id}`);
    var newHealthRecordEquivalence = new HealthRecordsEquivalence({
      oldHealthRecordId: healthRecordsId,
      newHealthRecordId: newHealthRecord._id
    });
    await newHealthRecordEquivalence.save();
  }
}

exports.healthRecordsMigration = healthRecordsMigration; 
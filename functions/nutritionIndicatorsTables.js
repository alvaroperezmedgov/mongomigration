const mongoose = require('mongoose');
const { pymsdb, newpymsdb } = require("../config/connections");
const { loggerNutritions } = require("../config/logger");
//const Schema = mongoose.Schema;

const nutritionSchema = new mongoose.Schema({}, { strict: false });
const newNutritionSchema = new mongoose.Schema({
  S: { type: String },
  M: { type: String },
  L: { type: String },
  sex: { type: String },
  indicator: { type: String },
  month: { type: Number }
}, { strict: false });

const NutritionIndicatorsTables = pymsdb.model('nutritionIndicatorsTables', nutritionSchema);
const NewNutrition = newpymsdb.model('nutritionIndicatorsTables', newNutritionSchema);

const delArr = ["_id", "createdAt", "updatedAt", "__v"];

const nutritionIndicatorsTablesMigration = async () => {
  console.log('nutritionIndicatorsTables');
  let nutritionIndicatorsTables = await NutritionIndicatorsTables.find().limit(50);
  console.log(nutritionIndicatorsTables);
  for (let index = 0; index < nutritionIndicatorsTables.length; index++) {

    const element = nutritionIndicatorsTables[index];
    console.log(element);
    let obj = JSON.parse(JSON.stringify(element));
    console.log(obj);
    let newData = {}
    for (const property in obj) {
      let tipo = typeof obj[property];
      if (tipo !== 'object') {
        newData[property] = obj[property];
      } else {
        if (property == 'coeff') {
          let coeffObj = JSON.parse(JSON.stringify(obj[property]));
          for (const coeffProperty in coeffObj) {
            newData[coeffProperty] = coeffObj[coeffProperty];
          }
        }
      }
    }
    console.log(newData);

    delArr.forEach(delElement => {
      delete newData[delElement];
    });

    var newNutrition = new NewNutrition(obj);
    newNutrition = await newNutrition.save();
    loggerNutritions.info({
      nutritionId: obj._id,
      newNutritionId: newNutrition._id,
      message: 'Creado'
    });
    console.log(`Migrate id ${obj._id} to ${newNutrition._id}`);
  }
}

exports.nutritionIndicatorsTablesMigration = nutritionIndicatorsTablesMigration;
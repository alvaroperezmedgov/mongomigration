const mongoose = require("mongoose");
const Personsf = require("../models/persons");
const Pool = require("pg").Pool;
const pool = new Pool({
  user: "postgres",
  host: "104.236.69.75",
  database: "prod_mias",
  password: "p4ssw0rd",
  port: 5432,
});

const filterEPS = (epsCode) => {
  const epsList = [
    {
      id: 2,
      code: "EPSS02",
      name:
        "SALUD TOTAL EPS DEL REGIMEN CONTRIBUTIVO Y DEL REGIMEN SUBSIDIADO SA",
      socialSecurityId: 3,
      nit: "800130907",
      isBillable: true,
    },
    {
      id: 3,
      code: "EPS025",
      name: "CAPRESOCA EPS",
      socialSecurityId: 3,
      nit: "891856000",
      isBillable: null,
    },
    {
      id: 4,
      code: "RES013",
      name: "UNARIÑO",
      socialSecurityId: 2,
      nit: "",
      isBillable: null,
    },
    {
      id: 5,
      code: "CCFC50",
      name: "COMFAORIENTE EPSS",
      socialSecurityId: 1,
      nit: "890500675",
      isBillable: null,
    },
    {
      id: 6,
      code: "EPSS08",
      name: "EPS COMPENSAR",
      socialSecurityId: 3,
      nit: "860066942",
      isBillable: null,
    },
    {
      id: 7,
      code: "EPSI01",
      name:
        "DUSAKAWI EPSI ASOCIACION DE CABILDOS INDIGENAS DEL CESAR Y GUAJIRA",
      socialSecurityId: 3,
      nit: "824001398",
      isBillable: null,
    },
    {
      id: 8,
      code: "ESS002",
      name:
        "EMPRESA MUTUAL PARA EL DESARROLLO INTEGRAL DE SALUD ESS EMDISALUD ESS",
      socialSecurityId: 3,
      nit: "811004055",
      isBillable: null,
    },
    {
      id: 9,
      code: "EPSIC2",
      name:
        "MANEXKA ASOCIACION DE CABILDOS DEL RESGUARDO INDIGENA ZENU DE SAN ANDRES DE SOTA",
      socialSecurityId: 1,
      nit: "812002376",
      isBillable: null,
    },
    {
      id: 10,
      code: "ESSC33",
      name: "COMPARTA ESE S COOPERATIVA DE SALUD COMUNITARIA",
      socialSecurityId: 1,
      nit: "804002105",
      isBillable: null,
    },
    {
      id: 11,
      code: "EPSS16",
      name: "COOMEVA EPS",
      socialSecurityId: 3,
      nit: "805000427",
      isBillable: true,
    },
    {
      id: 12,
      code: "EPSS10",
      name: "EPS Y MEDICINA PREPAGADA SURAMERICANA SA",
      socialSecurityId: 3,
      nit: "800088702-2",
      isBillable: true,
    },
    {
      id: 13,
      code: "CCF027",
      name: "CAJA DE COMPENSACION FAMILIAR DE NARIÑO",
      socialSecurityId: 3,
      nit: "891280008",
      isBillable: null,
    },
    {
      id: 14,
      code: "EPSS34",
      name: "CAPITAL SALUD EPS S SAS",
      socialSecurityId: 3,
      nit: "900298372",
      isBillable: null,
    },
    {
      id: 15,
      code: "EPS017",
      name: "EPS FAMISANAR SAS",
      socialSecurityId: 1,
      nit: "830003564",
      isBillable: null,
    },
    {
      id: 16,
      code: "EPS005",
      name: "ENTIDAD PROMOTORA DE SALUD SANITAS SA",
      socialSecurityId: 1,
      nit: "800251440",
      isBillable: true,
    },
    {
      id: 17,
      code: "EPS022",
      name: "CONVIDA EPS S",
      socialSecurityId: 3,
      nit: "899999107",
      isBillable: null,
    },
    {
      id: 18,
      code: "RES001",
      name: "FUERZAS MILITARES",
      socialSecurityId: 2,
      nit: "",
      isBillable: null,
    },
    {
      id: 19,
      code: "ESSC76",
      name: "ESS ASOCIACION MUTUAL BARRIOS UNIDOS DE QUIBDO",
      socialSecurityId: 1,
      nit: "818000140",
      isBillable: null,
    },
    {
      id: 20,
      code: "CCFC15",
      name: "CCF DE CORDOBA COMFACOR",
      socialSecurityId: 1,
      nit: "891080005",
      isBillable: null,
    },
    {
      id: 21,
      code: "EPSS05",
      name: "ENTIDAD PROMOTORA DE SALUD SANITAS SA",
      socialSecurityId: 3,
      nit: "800251440",
      isBillable: true,
    },
    {
      id: 22,
      code: "RES008",
      name: "UNISALUD",
      socialSecurityId: 2,
      nit: "",
      isBillable: null,
    },
    {
      id: 23,
      code: "CCFC07",
      name: "CCF DE CARTAGENA COMFAMILIAR CARTAGENA",
      socialSecurityId: 1,
      nit: "890480110",
      isBillable: null,
    },
    {
      id: 24,
      code: "ESS091",
      name: "EMPRESA PROMOTORA DE SALUD ECOOPSOS EPS SAS",
      socialSecurityId: 3,
      nit: "901093846",
      isBillable: null,
    },
    {
      id: 25,
      code: "EPSS23",
      name: "CRUZ BLANCA",
      socialSecurityId: 3,
      nit: "830009783",
      isBillable: true,
    },
    {
      id: 26,
      code: "EPSI05",
      name: "ASOCIACION MUTUAL ESS MALLAMAS",
      socialSecurityId: 3,
      nit: "837000084",
      isBillable: null,
    },
    {
      id: 27,
      code: "EPSS17",
      name: "EPS FAMISANAR SAS",
      socialSecurityId: 3,
      nit: "830003564",
      isBillable: null,
    },
    {
      id: 28,
      code: "EPSIC6",
      name: "PIJAOSALUD EPSI",
      socialSecurityId: 1,
      nit: "809008362",
      isBillable: null,
    },
    {
      id: 29,
      code: "CCF024",
      name: "CCF COMFAMILIAR HUILA",
      socialSecurityId: 3,
      nit: "891180008",
      isBillable: null,
    },
    {
      id: 30,
      code: "ESS133",
      name: "COMPARTA ESE S COOPERATIVA DE SALUD COMUNITARIA",
      socialSecurityId: 3,
      nit: "804002105",
      isBillable: null,
    },
    {
      id: 31,
      code: "EPS044",
      name: "MEDIMAS EPS SAS",
      socialSecurityId: 1,
      nit: "901097473",
      isBillable: true,
    },
    {
      id: 32,
      code: "RES002",
      name: "ECOPETROL",
      socialSecurityId: 2,
      nit: "",
      isBillable: null,
    },
    {
      id: 33,
      code: "EPS018",
      name: "SOS SERVICIO OCCIDENTAL DE SALUD",
      socialSecurityId: 1,
      nit: "805001157",
      isBillable: null,
    },
    {
      id: 34,
      code: "EPS008",
      name: "EPS COMPENSAR",
      socialSecurityId: 1,
      nit: "860066942",
      isBillable: null,
    },
    {
      id: 35,
      code: "EPS023",
      name: "CRUZ BLANCA",
      socialSecurityId: 1,
      nit: "830009783",
      isBillable: true,
    },
    {
      id: 36,
      code: "EPS040",
      name: "SAVIA SALUD EPS",
      socialSecurityId: 1,
      nit: "900604350-0",
      isBillable: true,
    },
    {
      id: 37,
      code: "EPS002",
      name:
        "SALUD TOTAL EPS DEL REGIMEN CONTRIBUTIVO Y DEL REGIMEN SUBSIDIADO SA",
      socialSecurityId: 1,
      nit: "800130907",
      isBillable: true,
    },
    {
      id: 38,
      code: "RE001",
      name: "REGIMEN EXCEPCIONES",
      socialSecurityId: 5,
      nit: "",
      isBillable: null,
    },
    {
      id: 39,
      code: "CCF015",
      name: "CCF DE CORDOBA COMFACOR",
      socialSecurityId: 3,
      nit: "891080005",
      isBillable: null,
    },
    {
      id: 40,
      code: "ESSC91",
      name: "EMPRESA PROMOTORA DE SALUD ECOOPSOS EPS SAS",
      socialSecurityId: 1,
      nit: "901093846",
      isBillable: null,
    },
    {
      id: 41,
      code: "CCFC55",
      name: "CCF DE ACOPI CAJACOPI  BARRANQUILLA",
      socialSecurityId: 1,
      nit: "890102044",
      isBillable: null,
    },
    {
      id: 42,
      code: "EPS033",
      name: "SALUDVIDA SA EPS",
      socialSecurityId: 1,
      nit: "830074184",
      isBillable: null,
    },
    {
      id: 43,
      code: "ESSC18",
      name: "SOCIEDAD SIMPLIFICADA POR ACCIONES EMSSANAR",
      socialSecurityId: 1,
      nit: "901021565-8",
      isBillable: null,
    },
    {
      id: 44,
      code: "EPSI06",
      name: "PIJAOSALUD EPSI",
      socialSecurityId: 3,
      nit: "809008362",
      isBillable: null,
    },
    {
      id: 45,
      code: "CCF007",
      name: "CCF DE CARTAGENA COMFAMILIAR CARTAGENA",
      socialSecurityId: 3,
      nit: "890480110",
      isBillable: null,
    },
    {
      id: 46,
      code: "EPSS45",
      name: "MEDIMAS EPS SAS",
      socialSecurityId: 3,
      nit: "901097473",
      isBillable: true,
    },
    {
      id: 47,
      code: "EPSC34",
      name: "CAPITAL SALUD EPS S SAS",
      socialSecurityId: 1,
      nit: "900298372",
      isBillable: null,
    },
    {
      id: 48,
      code: "EPS012",
      name: "COMFENALCO VALLE EPS",
      socialSecurityId: 1,
      nit: "890303093",
      isBillable: null,
    },
    {
      id: 49,
      code: "EPSIC1",
      name:
        "DUSAKAWI EPSI ASOCIACION DE CABILDOS INDIGENAS DEL CESAR Y GUAJIRA",
      socialSecurityId: 1,
      nit: "824001398",
      isBillable: null,
    },
    {
      id: 50,
      code: "ESS024",
      name: "COOSALUD ENTIDAD PROMOTORA DE SALUD SA",
      socialSecurityId: 3,
      nit: "900226715",
      isBillable: null,
    },
    {
      id: 51,
      code: "CCFC20",
      name: "CCF DEL CHOCO COMFACHOCO",
      socialSecurityId: 1,
      nit: "891600091",
      isBillable: null,
    },
    {
      id: 52,
      code: "RES011",
      name: "UATLANTICO",
      socialSecurityId: 2,
      nit: "",
      isBillable: null,
    },
    {
      id: 53,
      code: "RES010",
      name: "UCARTAGENA",
      socialSecurityId: 2,
      nit: "",
      isBillable: null,
    },
    {
      id: 54,
      code: "EPSC22",
      name: "CONVIDA EPS S",
      socialSecurityId: 1,
      nit: "899999107",
      isBillable: null,
    },
    {
      id: 55,
      code: "CCF102",
      name: "CCF DEL CHOCO COMFACHOCO",
      socialSecurityId: 3,
      nit: "891600091",
      isBillable: null,
    },
    {
      id: 56,
      code: "CCFC27",
      name: "CAJA DE COMPENSACION FAMILIAR DE NARIÑO",
      socialSecurityId: 1,
      nit: "891280008",
      isBillable: null,
    },
    {
      id: 57,
      code: "RES007",
      name: "UVALLE",
      socialSecurityId: 2,
      nit: "",
      isBillable: null,
    },
    {
      id: 58,
      code: "EPSC25",
      name: "CAPRESOCA EPS",
      socialSecurityId: 1,
      nit: "891856000",
      isBillable: null,
    },
    {
      id: 59,
      code: "ESSC24",
      name: "COOSALUD ENTIDAD PROMOTORA DE SALUD SA",
      socialSecurityId: 1,
      nit: "900226715",
      isBillable: null,
    },
    {
      id: 60,
      code: "CCF050",
      name: "COMFAORIENTE EPSS",
      socialSecurityId: 3,
      nit: "890500675",
      isBillable: null,
    },
    {
      id: 61,
      code: "CCF033",
      name: "CAJA DE COMPENSACION FAMILIAR DE SUCRE",
      socialSecurityId: 3,
      nit: "892200015",
      isBillable: null,
    },
    {
      id: 62,
      code: "EPSI03",
      name: "ESS ASOCIACIÓN INDIGENA DEL CAUCA AIC",
      socialSecurityId: 3,
      nit: "817001773",
      isBillable: null,
    },
    {
      id: 63,
      code: "EPS037",
      name: "NUEVA EPS",
      socialSecurityId: 1,
      nit: "900156264",
      isBillable: true,
    },
    {
      id: 64,
      code: "RES012",
      name: "UCORDOBA",
      socialSecurityId: 2,
      nit: "",
      isBillable: null,
    },
    {
      id: 65,
      code: "RES004",
      name: "MAGISTERIO",
      socialSecurityId: 2,
      nit: "",
      isBillable: null,
    },
    {
      id: 66,
      code: "ESSC02",
      name:
        "EMPRESA MUTUAL PARA EL DESARROLLO INTEGRAL DE SALUD ESS EMDISALUD ESS",
      socialSecurityId: 1,
      nit: "811004055",
      isBillable: null,
    },
    {
      id: 67,
      code: "EPS016",
      name: "COOMEVA EPS",
      socialSecurityId: 1,
      nit: "805000427",
      isBillable: true,
    },
    {
      id: 68,
      code: "EPSS12",
      name: "COMFENALCO VALLE EPS",
      socialSecurityId: 3,
      nit: "890303093",
      isBillable: null,
    },
    {
      id: 69,
      code: "EPSIC4",
      name: "EPS INDIGENA ANA WAYUU",
      socialSecurityId: 1,
      nit: "839000495",
      isBillable: null,
    },
    {
      id: 70,
      code: "CCFC23",
      name: "CCF COMFAMILIAR DE LA GUAJIRA",
      socialSecurityId: 1,
      nit: "892115006",
      isBillable: null,
    },
    {
      id: 71,
      code: "CCFC24",
      name: "CCF COMFAMILIAR HUILA",
      socialSecurityId: 1,
      nit: "891180008",
      isBillable: null,
    },
    {
      id: 72,
      code: "RES006",
      name: "CAPRUIS",
      socialSecurityId: 2,
      nit: "",
      isBillable: null,
    },
    {
      id: 73,
      code: "CCF053",
      name: "CCF DE CUNDINAMARCA COMFACUNDI",
      socialSecurityId: 3,
      nit: "860045904",
      isBillable: null,
    },
    {
      id: 74,
      code: "EPSS33",
      name: "SALUDVIDA SA EPS",
      socialSecurityId: 3,
      nit: "830074184",
      isBillable: null,
    },
    {
      id: 75,
      code: "EPSS40",
      name: "SAVIA SALUD EPS",
      socialSecurityId: 3,
      nit: "900604350-0",
      isBillable: true,
    },
    {
      id: 76,
      code: "RES005",
      name: "UANTIOQUIA",
      socialSecurityId: 2,
      nit: "",
      isBillable: null,
    },
    {
      id: 77,
      code: "ESS062",
      name: "ASOCIACION MUTUAL LA ESPERANZA ASMET SALUD",
      socialSecurityId: 3,
      nit: "900935126",
      isBillable: null,
    },
    {
      id: 78,
      code: "EPSS18",
      name: "SOS SERVICIO OCCIDENTAL DE SALUD",
      socialSecurityId: 3,
      nit: "805001157",
      isBillable: null,
    },
    {
      id: 79,
      code: "EPS010",
      name: "EPS Y MEDICINA PREPAGADA SURAMERICANA SA",
      socialSecurityId: 1,
      nit: "800088702-2",
      isBillable: true,
    },
    {
      id: 80,
      code: "ESS118",
      name: "SOCIEDAD SIMPLIFICADA POR ACCIONES EMSSANAR",
      socialSecurityId: 3,
      nit: "901021565-8",
      isBillable: null,
    },
    {
      id: 81,
      code: "EPSI04",
      name: "EPS INDIGENA ANA WAYUU",
      socialSecurityId: 3,
      nit: "839000495",
      isBillable: null,
    },
    {
      id: 82,
      code: "CCFC53",
      name: "CCF DE CUNDINAMARCA COMFACUNDI",
      socialSecurityId: 1,
      nit: "860045904",
      isBillable: null,
    },
    {
      id: 83,
      code: "ESSC07",
      name: "ASOCIACION MUTUAL SER",
      socialSecurityId: 1,
      nit: "806008394",
      isBillable: null,
    },
    {
      id: 84,
      code: "RES014",
      name: "UPTC",
      socialSecurityId: 2,
      nit: "",
      isBillable: null,
    },
    {
      id: 85,
      code: "ESSC62",
      name: "ASOCIACION MUTUAL LA ESPERANZA ASMET SALUD",
      socialSecurityId: 1,
      nit: "900935126",
      isBillable: null,
    },
    {
      id: 86,
      code: "EPSIC5",
      name: "ASOCIACION MUTUAL ESS MALLAMAS",
      socialSecurityId: 1,
      nit: "837000084",
      isBillable: null,
    },
    {
      id: 87,
      code: "CCF023",
      name: "CCF COMFAMILIAR DE LA GUAJIRA",
      socialSecurityId: 3,
      nit: "892115006",
      isBillable: null,
    },
    {
      id: 88,
      code: "ESS076",
      name: "ESS ASOCIACION MUTUAL BARRIOS UNIDOS DE QUIBDO",
      socialSecurityId: 3,
      nit: "818000140",
      isBillable: null,
    },
    {
      id: 89,
      code: "PPNA01",
      name: "POBLACION POBRE NO ASEGURADA",
      socialSecurityId: 4,
      nit: "890905211-1",
      isBillable: null,
    },
    {
      id: 90,
      code: "EPSS01",
      name: "ALIANSALUD ENTIDAD PROMOTORA DE SALUD",
      socialSecurityId: 3,
      nit: "830113831",
      isBillable: null,
    },
    {
      id: 91,
      code: "EPS001",
      name: "ALIANSALUD ENTIDAD PROMOTORA DE SALUD",
      socialSecurityId: 1,
      nit: "830113831",
      isBillable: null,
    },
    {
      id: 92,
      code: "EPSIC3",
      name: "ESS ASOCIACIÓN INDIGENA DEL CAUCA AIC",
      socialSecurityId: 1,
      nit: "817001773",
      isBillable: null,
    },
    {
      id: 93,
      code: "EPSS37",
      name: "NUEVA EPS",
      socialSecurityId: 3,
      nit: "900156264",
      isBillable: true,
    },
    {
      id: 94,
      code: "ESS207",
      name: "ASOCIACION MUTUAL SER",
      socialSecurityId: 3,
      nit: "806008394",
      isBillable: null,
    },
    {
      id: 95,
      code: "CCFC33",
      name: "CAJA DE COMPENSACION FAMILIAR DE SUCRE",
      socialSecurityId: 1,
      nit: "892200015",
      isBillable: null,
    },
    {
      id: 96,
      code: "CCF055",
      name: "CCF DE ACOPI CAJACOPI  BARRANQUILLA",
      socialSecurityId: 3,
      nit: "890102044",
      isBillable: null,
    },
  ].filter((elem) => elem.code == epsCode);
  return epsList != null && epsList[0] != null ? epsList[0].id: null;
};

const filterDocumentType = (key) => {
  switch (key) {
    case "CC":
      return 1;
    case "CE":
      return 2;
    case "CNV":
      return 3;
    case "PA":
      return 4;
    case "RC":
      return 5;
    case "TI":
      return 6;
    case "SD":
      return 7;
    case "PEP":
      return 8;
  }
};

const getInsuranceScheme = (key) => {
  switch (key) {
    case "Contributivo":
      return 1;
    case "Especial":
      return 2;
    case "Subsidiado":
      return 3;
    case "Excepciones":
      return 5;
    default:
      return 4;
  }
};

const personsMigration = async () => {
  let persons = await Personsf.find().skip(1).limit(200000);

  for (let index = 0; index < persons.length; index++) {
    const element = persons[index];

    let obj = JSON.parse(JSON.stringify(element));
    var newData = {};
    var socialSecurity = {};
    var phoneNumbers = {};

    for (const property in obj) {
      if (!Array.isArray(obj[property])) {
        let tipo = typeof obj[property];
        if (tipo !== "object") {
          newData[property] = obj[property];
        } else {
          switch (property) {
            case "socialSecurity":
              socialSecurity = obj[property];
              break;
            case "phoneNumbers":
              phoneNumbers = obj[property];
              break;
            default:
              break;
          }
        }
      } else {
        switch (property) {
          case "socialSecurity":
            socialSecurity = obj[property];
            break;
          case "phoneNumbers":
            phoneNumbers = obj[property];
            break;
          default:
            break;
        }
      }
    }

    const eps = filterEPS(socialSecurity.epsCode);
    const documentTypeId = filterDocumentType(newData.documentType);
    const genderId = newData.sex == "F" || newData.sex == 1 ? 1 : 2;
    const zoneId = (newData.zone = "U" || "urbana" || "urbana" ? 1 : 2);
    const commune = (newData.commune = 33 ? null : newData.commune);
    const birthdate =
      newData.birthdate && newData.birthdate != "null"
        ? newData.birthdate
        : "1900-01-01";
    const insuranceScheme = getInsuranceScheme(socialSecurity.insuranceScheme);
    const fullad = newData.address != null ? newData.address.replace("'", "").replace("*", " "): '';
    const query = `INSERT INTO public."TBL_PERSON" ("idPerson", "documentTypeId", "documentNumber", "firstName", "secondName", "firstLastName", "secondLastName", "birthday", "genderId", "countryBirthId", "zoneId", "communeId", "neighborhoodId", "phone", "cellphone", "otherPhone", "eapbId", "socialSecurityId", "fullAddress") VALUES  (DEFAULT, ${documentTypeId}, '${
      newData.document
    }', '${newData.firstname ? newData.firstname.replace("'", "") : ""}', '${
      newData.middlename ? newData.middlename.replace("'", "") : ""
    }', '${newData.lastname.replace("'", "")}', '${
      newData.secondLastname ? newData.secondLastname.replace("'", "") : ""
    }', '${birthdate}', ${genderId}, null, ${zoneId}, ${commune}, null, '${
      phoneNumbers.phone ? phoneNumbers.phone : ""
    }', '${phoneNumbers.cellphone ? phoneNumbers.cellphone : ""}', '${
      phoneNumbers.other ? phoneNumbers.other : ""
    }', ${eps != null ? eps : null}, ${insuranceScheme}, '${fullad}')`
      .replace("undefined", null)
      .replace("URBANA", null)
      .replace("RURAL", null);

    pool.query(query, (error, results) => {
      if (error) {
        console.log(element._id);
        throw error
      }
      console.log(query);
      console.log(index)
    });
  }
};

exports.personsMigration = personsMigration;

const SurveysEquivalence = require('../models/surveysEquivalence');
const PersonsEquivalence = require('../models/personsEquivalence');
const EntitiesEquivalence = require('../models/entitiesEquivalence');
const HealthRecordsEquivalence = require('../models/healthRecordsEquivalence');
const NewBillings = require('../models/newBillings');
const NewCanalizations = require('../models/newCanalizations');
const NewHealthRecords = require('../models/newHealthRecords');
const NewSurveys = require('../models/newSurveys');
const PersonsSocialSecurity = require('../models/personsSocialSecurity')

const standardizeData = async () => {
  console.log("surveysEquivalences");
  let surveysEquivalences = await SurveysEquivalence.find();
  for (let index = 0; index < surveysEquivalences.length; index++) {
    const element = surveysEquivalences[index];
    let obj = JSON.parse(JSON.stringify(element));
    try {
      const res = await NewCanalizations.updateMany(
        { survey: obj.oldSurveyId },
        { $set: { survey: obj.newSurveyId } }
      );
      //console.log(res);
    } catch (e) {
      print(e);
    }
    try {
      const res = await NewHealthRecords.updateMany(
        { survey: obj.oldSurveyId },
        { $set: { survey: obj.newSurveyId } }
      );
      //console.log(res);
    } catch (e) {
      print(e);
    }
  }
  console.log("personsEquivalence");
  let personsEquivalence = await PersonsEquivalence.find();
  for (let index = 0; index < personsEquivalence.length; index++) {
    const element = personsEquivalence[index];
    let obj = JSON.parse(JSON.stringify(element));
    try {
      const res = await NewBillings.updateMany(
        { person: obj.oldPersonId },
        { $set: { person: obj.newPersonId } }
      );
      //console.log(res);
    } catch (e) {
      print(e);
    }
    try {
      const res = await NewCanalizations.updateMany(
        { person: obj.oldPersonId },
        { $set: { person: obj.newPersonId } }
      );
      //console.log(res);
    } catch (e) {
      print(e);
    }
    try {
      const res = await NewHealthRecords.updateMany(
        { person: obj.oldPersonId },
        { $set: { person: obj.newPersonId } }
      );
      //console.log(res);
    } catch (e) {
      print(e);
    }
    try {
      const res = await NewSurveys.updateMany(
        { person: obj.oldPersonId },
        { $set: { person: obj.newPersonId } }
      );
      //console.log(res);
    } catch (e) {
      print(e);
    }
  }
  let entitiesEquivalence = await EntitiesEquivalence.find();
  for (let index = 0; index < entitiesEquivalence.length; index++) {
    const element = entitiesEquivalence[index];
    let obj = JSON.parse(JSON.stringify(element));
    try {
      const res = await NewCanalizations.updateMany(
        { eps: obj.oldEntityId },
        { $set: { eps: obj.newEntityId } }
      );
      const res2 = await NewCanalizations.updateMany(
        { appointmentIps: obj.oldEntityId },
        { $set: { appointmentIps: obj.newEntityId } }
      );
      //console.log(res);
      //console.log(res2);
    } catch (e) {
      print(e);
    }
    try {
      const res = await PersonsSocialSecurity.updateMany(
        { eps: obj.oldEntityId },
        { $set: { eps: obj.newEntityId } }
      );
      //console.log(res);
    } catch (e) {
      print(e);
    }
    try {
      const res = await NewSurveys.updateMany(
        { entity: obj.oldEntityId },
        { $set: { eps: obj.newEntityId } }
      );
      //console.log(res);
    } catch (e) {
      print(e);
    }
  }
  console.log("healthRecordsEquivalence");
  let healthRecordsEquivalence = await HealthRecordsEquivalence.find();
  for (let index = 0; index < healthRecordsEquivalence.length; index++) {
    const element = healthRecordsEquivalence[index];
    let obj = JSON.parse(JSON.stringify(element));
    try {
      const res = await NewBillings.updateMany(
        { healthRecordId: obj.oldHealthRecordId },
        { $set: { healthRecordId: obj.newHealthRecordId } }
      );
      //console.log(res);
    } catch (e) {
      print(e);
    }
  }
  console.log('end');
}
exports.standardizeData = standardizeData;
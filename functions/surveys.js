const mongoose = require('mongoose');
const { pymsdb, newpymsdb } = require("../config/connections");
const { loggerSurveys } = require("../config/logger");
const Persons = require('../models/persons');
const Entities = require('../models/entities');
const NewSurveys = require('../models/newSurveys');
const SurveysEquivalence = require('../models/surveysEquivalence');
const Schema = mongoose.Schema;

const surveysSchema = new mongoose.Schema({}, { strict: false });
const surveysHealthRecordsSchema = new Schema({
  surveyId: { type: Schema.Types.ObjectId, ref: 'surveys', index: true },
  healthRecordId: { type: Schema.Types.ObjectId, ref: 'healthRecords', index: true },
}, { strict: false });

//estos son solo 3 documentos
const surveysBriefInterventionsSchema = new Schema({
  surveyId: { type: Schema.Types.ObjectId, ref: 'surveys', index: true },
  briefInterventionType: { type: String }, //este es el texto de l propiedad contenedora de los datos briefIntervention1 o briefIntervention2 ...
  tipoOrientacion: { type: String },
  desarrollo: { type: String },
  logros: { type: String },
}, { strict: false });

const surveysExplanatoryNotesSchema = new Schema({
  surveyId: { type: Schema.Types.ObjectId, ref: 'surveys', index: true },
  explanatoryNote: { type: String }
}, { strict: false });

const surveysCollaboratorsSchema = new Schema({
  surveyId: { type: Schema.Types.ObjectId, ref: 'surveys', index: true },
  collaborator: { type: String }
}, { strict: false });

const surveyCanalizationsSchema = new Schema({
  surveyId: { type: Schema.Types.ObjectId, ref: 'surveys', index: true },
  canalizationId: { type: Schema.Types.ObjectId, ref: 'canalizations', index: true },
}, { strict: false });

const surveyPersonCharacterizationSchema = new Schema({
  surveyId: { type: Schema.Types.ObjectId, ref: 'surveys', index: true },
  educationLevel: { type: String },
  days: { type: Number },
  months: { type: Number },
  years: { type: Number },
  group: { type: String },
  weight: { type: Number },
  height: { type: Number },
  bmi: { type: Number },
  bfa: { type: Number },
  hfa: { type: Number },
}, { strict: false });

const surveyPersonCharacterizationIndicatorsSchema = new Schema({
  surveyPersonCharacterizationId: { type: Schema.Types.ObjectId, ref: 'surveyPersonCharacterization', index: true },
  indicatorOutcomes: { type: String },
}, { strict: false });

const SurveysDataSchema = new Schema({
  surveyId: { type: Schema.Types.ObjectId, ref: 'surveys', index: true },
  data: { type: Object },
}, { strict: false });

const Surveys = pymsdb.model('surveys', surveysSchema);
const SurveysHealthRecords = newpymsdb.model('surveysHealthRecords', surveysHealthRecordsSchema);
const SurveysExplanatoryNotes = newpymsdb.model('surveysExplanatoryNotes', surveysExplanatoryNotesSchema);
const SurveysCollaborators = newpymsdb.model('surveysCollaborators', surveysCollaboratorsSchema);
const SurveysCanalizations = newpymsdb.model('surveysCanalizations', surveyCanalizationsSchema);
const SurveyPersonCharacterization = newpymsdb.model('surveysPersonCharacterizations', surveyPersonCharacterizationSchema);
const SurveyPersonCharacterizationIndicators = newpymsdb.model('surveysPersonCharacterizationIndicators', surveyPersonCharacterizationIndicatorsSchema);
const SurveysBriefInterventions = newpymsdb.model('surveysBriefInterventions', surveysBriefInterventionsSchema);
const SurveysData = newpymsdb.model('surveysDatas', SurveysDataSchema);

const delArr = ["_id", "createdAt", "updatedAt", "__v"];

const surveysMigration = async () => {
  console.log('surveys');
  let surveys = await Surveys.find().limit(100);
  for (let index = 0; index < surveys.length; index++) {
    const element = surveys[index];
    let obj = JSON.parse(JSON.stringify(element));
    let person = await Persons.findById(obj.person._id);
    let entity = await Entities.findById(obj.entity._id);
    let newData = {}
    let objectsToMigrate = {}
    let indicatorOutcomesArr = []
    let dataToMigrate = {};
    let idPerson;
    let idEntity;

    if (person) {
      idPerson = obj.person._id;
    } else {
      var newPerson = new Persons(obj.person);
      newPerson = await newPerson.save();
      idPerson = newPerson._id;
    }
    newData.person = idPerson;

    if (entity) {
      idEntity = obj.entity._id;
    } else {
      var newEntity = new Entities(obj.entity);
      newEntity = await newEntity.save();
      idEntity = newEntity._id;
    }
    newData.entity = idEntity;

    for (const property in obj) {
      let tipo = typeof obj[property];
      if (tipo !== 'object') {
        newData[property] = obj[property];
      } else {
        switch (property) {
          case "healthRecords":
          case "explanatoryNotes":
          case "collaborators":
          case "canalizations":
          case "personCharacterization":
          case "briefInterventions":
            objectsToMigrate[property] = obj[property];
            break;
          case "data":
            dataToMigrate = obj[property];
            break;
          case "entity":
          case "person":
            //nada
            break;
          default:
            loggerSurveys.error({
              id: obj._id,
              property: property,
              message: 'Valor no organizado'
            });
            break;
        }
      }
    }
    /* console.log(dataToMigrate); */
    delArr.forEach(delElement => {
      delete newData[delElement];
    });

    /* console.log(newData);
    console.log(objectsToMigrate); */

    var newSurvey = new NewSurveys(newData);
    newSurvey = await newSurvey.save();

    loggerSurveys.info({
      surveyId: obj._id,
      newSurveyId: newSurvey._id,
      message: 'Creado'
    });
    console.log(`Migrate id ${obj._id} to ${newSurvey._id}`);

    var newSurveyEquivalence = new SurveysEquivalence({
      oldSurveyId: obj._id,
      newSurveyId: newSurvey._id
    });
    await newSurveyEquivalence.save();

    if (Object.keys(dataToMigrate).length) {
      let newDataObj = {
        surveyId: newSurvey._id,
        data: dataToMigrate
      }
      var newSurveysData = new SurveysData(newDataObj);
      await newSurveysData.save();
    }

    if (Object.keys(objectsToMigrate).length) {
      for (const key in objectsToMigrate) {
        if (objectsToMigrate.hasOwnProperty(key)) {
          const objectsToMigrateElement = objectsToMigrate[key];
          //objectsToMigrateElement.surveysId = newSurvey._id;
          if (Object.keys(objectsToMigrateElement).length) {
            switch (key) {
              case "healthRecords":
                objectsToMigrateElement.forEach(async (healthRecordsElement) => {
                  let healthRecordsObj = {
                    surveyId: newSurvey._id,
                    healthRecordId: healthRecordsElement
                  }
                  var newsurveysHealthRecords = new SurveysHealthRecords(healthRecordsObj);
                  await newsurveysHealthRecords.save();
                });
                break;
              case "explanatoryNotes":
                objectsToMigrateElement.forEach(async (explanatoryNotesElement) => {
                  let explanatoryNotesObj = {
                    surveyId: newSurvey._id,
                    explanatoryNote: explanatoryNotesElement
                  }
                  var newSurveysExplanatoryNotes = new SurveysExplanatoryNotes(explanatoryNotesObj);
                  await newSurveysExplanatoryNotes.save();
                });
                break;
              case "collaborators":
                objectsToMigrateElement.forEach(async (collaboratorsElement) => {
                  let collaboratorsObj = {
                    surveyId: newSurvey._id,
                    collaborator: collaboratorsElement
                  }
                  var newSurveysCollaborators = new SurveysCollaborators(collaboratorsObj);
                  await newSurveysCollaborators.save();
                });
                break;
              case "canalizations":
                objectsToMigrateElement.forEach(async (canalizationsElement) => {
                  let canalizationsObj = {
                    surveyId: newSurvey._id,
                    canalizationId: canalizationsElement
                  }
                  var newSurveysCanalization = new SurveysCanalizations(canalizationsObj);
                  await newSurveysCanalization.save();
                });
                break;
              case "personCharacterization":
                var personCharacterization = {
                  surveyId: newSurvey._id,
                }
                for (const key1 in objectsToMigrateElement) {
                  switch (key1) {
                    case "demographics":
                      const demographicObj = objectsToMigrateElement[key1];
                      for (const demographicObjKey in demographicObj) {
                        if (demographicObj.hasOwnProperty(demographicObjKey)) {
                          const demelement = demographicObj[demographicObjKey];
                          if (demographicObjKey == "age") {
                            for (const key2 in demographicObj.age) {
                              if (demographicObj.age.hasOwnProperty(key2)) {
                                const element2 = demographicObj.age[key2];
                                personCharacterization[key2] = element2
                              }
                            }
                          } else {
                            personCharacterization[demographicObjKey] = demelement
                          }
                        }
                      }
                      break;
                    case "dimension":
                      const dimensionObj = objectsToMigrateElement[key1]
                      for (const dimensionKey in dimensionObj) {
                        if (dimensionObj.hasOwnProperty(dimensionKey)) {
                          const dimelement = dimensionObj[dimensionKey];
                          if (dimensionKey == "indicatorsOutcomes") {
                            indicatorOutcomesArr = dimelement;
                            //se debe recorrer ese arreglo y llevar a nueva coleccion
                          } else {
                            personCharacterization[dimensionKey] = dimelement;
                          }
                        }
                      }
                      break;
                    default:
                      loggerSurveys.error({
                        id: obj._id,
                        key: key1,
                        message: 'Valor no organizado personCharacterization'
                      });
                      break;
                  }
                }
                //console.log(personCharacterization);
                var newPersonCharacterization = new SurveyPersonCharacterization(personCharacterization);
                newPersonCharacterization = await newPersonCharacterization.save();
                if (indicatorOutcomesArr.length) {
                  indicatorOutcomesArr.forEach(async indicatorOutcomesElement => {
                    let indicatorOutcomesObj = {
                      surveyPersonCharacterizationId: newPersonCharacterization._id,
                      indicatorOutcomes: indicatorOutcomesElement
                    }
                    var newIndicatorOutcomes = new SurveyPersonCharacterizationIndicators(indicatorOutcomesObj);
                    await newIndicatorOutcomes.save();
                  });
                }
                break;
              case "briefInterventions":
                for (const briefInterventionskey in objectsToMigrateElement) {
                  if (objectsToMigrateElement.hasOwnProperty(briefInterventionskey)) {
                    var briefInterventions = {
                      surveyId: newSurvey._id,
                      briefInterventionType: briefInterventionskey
                    }
                    const briefInterventionsElement = objectsToMigrateElement[briefInterventionskey];
                    for (const briefInterventionsElementKey in briefInterventionsElement) {
                      if (briefInterventionsElement.hasOwnProperty(briefInterventionsElementKey)) {
                        briefInterventions[briefInterventionsElementKey] = briefInterventionsElement[briefInterventionsElementKey];
                      }
                    }
                    var newBriefInterventions = new SurveysBriefInterventions(briefInterventions);
                    await newBriefInterventions.save();
                  }
                }
                break;

              default:
                loggerSurveys.error({
                  id: obj._id,
                  key: key,
                  message: 'Valor no organizado objectsToMigrateElement'
                });
                break;
            }
          }
        }
      }
    }
  }
}

exports.surveysMigration = surveysMigration;
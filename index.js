const config = require('./config')
const mongoose = require('mongoose')
const { entitiesMigration } = require("./functions/entities");
const { personsMigration } = require("./functions/persons");
const { billingsMigration } = require('./functions/billings');
const { canalizationsMigration } = require('./functions/canalizations');
const { nutritionIndicatorsTablesMigration } = require('./functions/nutritionIndicatorsTables');
const { contractsMigration } = require('./functions/contracts');
const { healthRecordsMigration } = require('./functions/healthRecords');
const { surveysMigration } = require('./functions/surveys');
const { standardizeData } = require('./functions/standardizeData');
const { equalCollections } = require('./functions/equalCollections')

mongoose.connect(`mongodb://${config.url}:${config.port}/${config.database}`, { useCreateIndex: true, useNewUrlParser: true, useUnifiedTopology: true, useFindAndModify: false })
  .catch(e => {
    console.log(e)
  })
mongoose.connection.on('connected', () => {
  console.log(`[Mongoose]: connection open to ${config.url}/${config.database}`)
  migrate();
})
mongoose.connection.on('error', (e) => {
  console.log(`[Mongoose]: connection error: ${e}`)
})
mongoose.connection.on('disconnected', () => {
  console.log('[Mongoose]: connection disconnected')
})

async function migrate() {
  await personsMigration();
}
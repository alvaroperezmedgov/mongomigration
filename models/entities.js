const mongoose = require('mongoose')
const { pymsdb } = require("../config/connections");
var Schema = mongoose.Schema;

const entitiesSchema = new Schema({}, { strict: false });

module.exports = pymsdb.model('entities', entitiesSchema);
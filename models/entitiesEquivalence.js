const mongoose = require('mongoose');
const { newpymsdb } = require("../config/connections");

const entitiesEquivalenceSchema = new mongoose.Schema({
  oldEntityId: { type: mongoose.Schema.Types.ObjectId, ref: 'entities', index: true },
  newEntityId: { type: mongoose.Schema.Types.ObjectId, ref: 'newEntities', index: true },
}, { strict: false });

module.exports = newpymsdb.model('entitiesEquivalence', entitiesEquivalenceSchema);
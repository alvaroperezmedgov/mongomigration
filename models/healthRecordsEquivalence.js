const mongoose = require('mongoose')
const { newpymsdb } = require("../config/connections");

const healthRecordsEquivalenceSchema = new mongoose.Schema({
  oldHealthRecordId: { type: mongoose.Schema.Types.ObjectId, ref: 'healthRecords', index: true },
  newHealthRecordId: { type: mongoose.Schema.Types.ObjectId, ref: 'newHealthRecords', index: true },
}, { strict: false });

module.exports = newpymsdb.model('healthRecordsEquivalence', healthRecordsEquivalenceSchema);
const mongoose = require('mongoose')
var Schema = mongoose.Schema;
const { newpymsdb } = require("../config/connections");

const newBillingsSchema = new mongoose.Schema({
  HCType: { type: String },
  epsCode: { type: String, default: 'NA' },
  name: { type: String, default: 'NA' },
  CUPS: { type: String, default: 'NA' },
  SAFIX: { type: String, default: 'NA' },
  CUMS: { type: String },
  qty: { type: Number, default: 1 },
  type: { type: String, default: 'NA' },
  costCenter: { type: String, default: 'NA' },
  contractCode: { type: String, default: 'NA' },
  entityCode: { type: String, default: 'NA' },
  billingNit: { type: String, default: 'NA' },
  eps: { type: String, default: 'NA' },
  epsNit: { type: String, default: 'NA' },
  environment: { type: String, },
  dimension: { type: String, },
  professionalCode: { type: String, },
  attentionDate: { type: Date, },
  completionDate: { type: Date, },
  placeOfAttention: { type: String, },
  addressOfAttention: { type: String, },
  internalConsent1: { type: String },
  internalConsent2: { type: String },
  healthRecordId: { type: Schema.Types.ObjectId, ref: 'healthRecords', index: true },
  levelCode: { type: String, },
  person: { type: Schema.Types.ObjectId, ref: 'persons', index: true },
}, { strict: false, timestamps: true });

module.exports = newpymsdb.model('billings', newBillingsSchema);
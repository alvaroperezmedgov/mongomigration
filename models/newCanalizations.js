const mongoose = require('mongoose')
const { newpymsdb } = require("../config/connections");
var Schema = mongoose.Schema;

const newCanalizationsSchema = new mongoose.Schema({
  survey: { type: Schema.Types.ObjectId, ref: 'surveys', required: true },
  eps: { type: Schema.Types.ObjectId, ref: 'entities' },
  person: { type: Schema.Types.ObjectId, ref: 'persons', required: true },
  maxTimeEpsResponse: { type: Date },
  maxTimeAppointment: { type: Date },
  attentionDate: { type: Date },
  completionDate: { type: Date },
  createdBy: { type: String },
  //risk
  riskCanalizationType: { type: String },
  riskDescription: { type: String },
  riskType: { type: String },
  riskrn: { type: Number },
  //appointment
  appointmentIps: { type: Schema.Types.ObjectId, ref: 'entities' },
  appointmentDate: { type: Date },
  appointmentEpsResponseDate: { type: Date },
  appointmentProfessionalName: { type: String },
  appointmentAttended: { type: Boolean },
  //responsible
  responsibleFullname: { type: String },
  responsibleDocumentType: { type: String },
  responsibleDocument: { type: String },
  responsibleRelationship: { type: String },
  responsiblePhone: { type: String },
  //unattended
  unattendedReason: { type: String },
  unattendedObservation: { type: String },
}, { strict: false, timestamps: true });

module.exports = newpymsdb.model('canalizations', newCanalizationsSchema);
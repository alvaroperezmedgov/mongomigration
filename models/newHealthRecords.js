const mongoose = require('mongoose');
const { newpymsdb } = require("../config/connections");
var Schema = mongoose.Schema;

const newHealthRecordsSchema = new Schema({
  type: { type: String },
  referredBy: { type: String },
  person: { type: Schema.Types.ObjectId, ref: 'persons' },
  survey: { type: Schema.Types.ObjectId, ref: 'surveys' },
  createdBy: { type: String },
  createdByFullName: { type: String },
  createdByDocument: { type: String }, // Concatenar CC*1128455
  signature: { type: Schema.Types.ObjectId, ref: 'signatures' },
  attentionDate: { type: Date },
  completionDate: { type: Date },
  attachmentDate: { type: Date },
  //reasonConsultation
  reasonConsultation: { type: String },
  currentIllness: { type: String },
  //process
  makeTest: { type: String },
  testResult: { type: String },
  processDescription: { type: String },
}, { strict: false, timestamps: true });

module.exports = newpymsdb.model('healthRecords', newHealthRecordsSchema);
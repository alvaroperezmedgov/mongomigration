const mongoose = require('mongoose')
const { newpymsdb } = require("../config/connections");
var Schema = mongoose.Schema;

const newSurveysSchema = new mongoose.Schema({
  dimension: { type: String, required: true },
  entity: { type: Schema.Types.ObjectId, ref: 'entities', index: true },
  environment: { type: String, required: true },
  person: { type: Schema.Types.ObjectId, ref: 'persons', index: true },
  visitIndex: { type: Number, required: true },
  state: { type: String, default: 'open' },
  sn: { type: Number, default: 1 },
  createdBy: { type: String, required: true },
  endedBy: { type: String },
  completionReason: { type: String },
  endedDate: { type: Date },
}, { strict: false, timestamps: true });

module.exports = newpymsdb.model('surveys', newSurveysSchema);
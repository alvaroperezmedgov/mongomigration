const mongoose = require('mongoose');
const { pymsdb } = require("../config/connections");
var Schema = mongoose.Schema;

const personsSchema = new Schema({}, { strict: false });

module.exports = pymsdb.model('persons', personsSchema);
const mongoose = require('mongoose');
const { newpymsdb } = require("../config/connections");
const Schema = mongoose.Schema;

const personsEquivalenceSchema = new Schema({
  oldPersonId: { type: Schema.Types.ObjectId, ref: 'persons', index: true },
  newPersonId: { type: Schema.Types.ObjectId, ref: 'newPersons', index: true },
}, { strict: false });

module.exports = newpymsdb.model('personsEquivalences', personsEquivalenceSchema);
const mongoose = require('mongoose');
const { newpymsdb } = require("../config/connections");
var Schema = mongoose.Schema;

const personsSocialSecuritySchema = new mongoose.Schema({
  eps: { type: Schema.Types.ObjectId, ref: 'entities', index: true },
}, { strict: false });

module.exports = newpymsdb.model('personsSocialSecurities', personsSocialSecuritySchema);
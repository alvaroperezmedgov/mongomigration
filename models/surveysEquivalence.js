const mongoose = require('mongoose');
const { newpymsdb } = require("../config/connections");
const Schema = mongoose.Schema;

const surveysEquivalenceSchema = new Schema({
  oldSurveyId: { type: mongoose.Schema.Types.ObjectId, ref: 'surveys', index: true },
  newSurveyId: { type: mongoose.Schema.Types.ObjectId, ref: 'newSurveys', index: true },
}, { strict: false });

module.exports = newpymsdb.model('surveysEquivalences', surveysEquivalenceSchema);